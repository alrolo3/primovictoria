package com.gmu.primovictoria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimovictoriaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrimovictoriaApplication.class, args);
	}

}
